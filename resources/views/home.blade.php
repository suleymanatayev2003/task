<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Task</title>
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
  <link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
</head>
<body>
<section id="formSection">
  <div class="container">
    <div class="logo">
      <img src="{{ asset('img/hh.png') }}" alt="logo" class="h-100 w-100">
    </div>

    <div id="form">
      <div id="mail">
        <img src="{{ asset('img/mail.png') }}" alt="mail" class="h-100 w-100">
      </div>

      <form autocomplete="off" id="commentForm">
        <div class="row">
          <div class="col-md-6 cpr-3">
            <div class="form-group" >
              <label for="name" class="required">Имя</label>
              <input type="text" name="name" class="form-control" required id="name" maxlength="30">
            </div>
            <div class="form-group mb-0 sm-mb-3">
              <label for="email" class="required">E-Mail</label>
              <input type="email" name="email" class="form-control" required id="email" maxlength="30">
            </div>
          </div>
          <div class="col-md-6 cpl-3">
            <div class="form-group" id="bodyGroup">
              <label for="body" class="required">Комментарий</label>
              <textarea name="body" class="form-control" required id="body" maxlength="200"></textarea>
            </div>
          </div>
        </div>
        <div class="text-right mt-5" id="formButton">
          <button type="submit" class="btn btn-danger">Записать</button>
        </div>
      </form>
    </div>
  </div>
</section>
<section id="comments">
  <div class="container">
    <h1 class="text-center">Выводим комментарии</h1>

    <div class="row">
      <div id="newComment"></div>
      @foreach($comments as $comment)
        <div class="col-lg-4 col-md-6 col-sm-12 comment-card">
          <div class="comment text-center h-100">
            <h2>{{ $comment->name }}</h2>
            <div class="p-4">
              <h4>{{ $comment->email }}</h4>
              <p class="mt-5">{{ $comment->body }}</p>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</section>

<section id="footer">
  <div class="container d-flex justify-content-between align-items-center" id="footerContent">
    <div class="logo">
      <img src="{{ asset('img/hh.png') }}" alt="logo" class="h-100 w-100">
    </div>
    <div id="contact">
      <a href="https://facebook.com" target="_blank" class="mr-4">
        <img src="{{ asset('img/facebook.png') }}" alt="facebook">
      </a>
      <a href="https://vk.com" target="_blank">
        <img src="{{ asset('img/vk.png') }}" alt="vk">
      </a>
    </div>
  </div>
</section>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>

<script>
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

  $('#commentForm input[name=name]').on('input', function () {
    this.value = this.value.replace(/[^a-zA-Z]/g,'');
  });
  $('#commentForm input[name=email]').on('input', function () {
    this.value = this.value.replace(/[^a-zA-Z0-9@.]/g,'');
  });
  $('#commentForm textarea[name=body]').on('input', function () {
    this.value = this.value.replace(/[^a-zA-Z0-9!?., '"]/g,'');
  });

  $('#commentForm').on('submit', function (e) {
    e.preventDefault();

    let button = $('#formButton button');
    let name = $('#commentForm input[name=name]');
    let email = $('#commentForm input[name=email]');
    let body = $('#commentForm textarea[name=body]');

    if(name.val().length > 30 || email.val().length > 30 || body.val().length > 200) {
      return;
    }

    button.attr('disabled', true);

    axios({
      method: 'post',
      url: '{{ route('comments.store') }}',
      data: {
        name: name.val(),
        email: email.val(),
        body: body.val(),
      }
    }).then((response) => {
      Toast.fire({
        icon: 'success',
        title: 'Комментарий отправить!'
      });

      let html = '<div class="col-lg-4 col-md-6 col-sm-12 mt-5 comment-card"><div class="comment text-center h-100"><h2>' + name.val() + '</h2><div class="p-4"><h4>' + email.val() + '</h4><p class="mt-5">' + body.val() + '</p></div></div></div>';

      $('#newComment').after(html);

      name.val(null);
      email.val(null);
      body.val(null);
      button.attr('disabled', false);
    }, (error) => {
      Toast.fire({
        icon: 'error',
        title: 'Произошла ошибка!'
      });

      name.val(null);
      email.val(null);
      body.val(null);
      button.attr('disabled', false);
    });
  });
</script>
</body>
</html>
